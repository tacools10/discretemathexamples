package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThomasCools on 9/30/18.
 */
public class IntegerMath {

    public String Add(String a, String b, int base) {
        int c = 0;
        StringBuilder sum = new StringBuilder();
        if (a.length() > b.length()) {
            StringBuilder b1 = new StringBuilder(b);
            b1.reverse();
            while (b1.length() < a.length()) {
                b1.append(0);
            }
            b = b1.reverse().toString();
        } else if (a.length() < b.length()) {
            StringBuilder a1 = new StringBuilder(a);
            a1.reverse();
            while (a1.length() < b.length()) {
                a1.append(0);
            }
            a = a1.reverse().toString();
        }

        System.out.println("a is: " + a);
        System.out.println("b is: " + b);

        for (int i = a.length() - 1; i >= 0; i--) {
            int d = (Character.getNumericValue(a.charAt(i)) + Character.getNumericValue(b.charAt(i)) + c) / base;
            sum.append((Character.getNumericValue(a.charAt(i)) + Character.getNumericValue(b.charAt(i)) + c - (base * d)));
            c = d;
        }
        if (c != 0) {
            sum.append(c);
        }
        sum.reverse();
        return sum.toString();
    }

    public String Multiply(String a, String b, int base) {

        ArrayList<String> products = new ArrayList<>();
        if (a.length() < b.length()) {
            throw new IllegalArgumentException("a must be equal to or longer in length than b");
        }
        StringBuilder indProduct = new StringBuilder();
        for (int i = b.length() - 1; i >= 0; i--) {
            for (int j = a.length() - 1 + (b.length() - i - 1); j >= 0; j--) {
                if (j > a.length() - 1) {
                    indProduct.append(0);
                } else {
                    if (Character.getNumericValue(b.charAt(i)) == 1) {
                        indProduct.append(a.charAt(j));
                    } else {
                        indProduct.append(0);
                    }
                }
            }
            products.add(indProduct.reverse().toString());
            indProduct.delete(0, indProduct.length());
        }
        String solution = "";
        for (int i = 1; i < products.size(); i++) {
            if (i == 1) {
                solution = Add(products.get(i - 1), products.get(i), base);
                System.out.println("stage 1 solution: " + solution);
            } else {
                System.out.println("state 1 solution is: " + solution + " to add is " + products.get(i));
                solution = Add(solution, products.get(i), base);

            }

        }
        return solution;

    }


    public static void main(String[] args) {
        int base = 2;
        String a = "110";
        String b = "11000";
        String c = "110";
        String d = "101";
        IntegerMath test = new IntegerMath();
        System.out.println(test.Add(a, b, base));
        System.out.println(test.Multiply(c, d, base));


    }


}
