package test;

import main.IntegerMath;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by ThomasCools on 9/30/18.
 */

public class IntegerMathTest {

    @Test
    public void shouldGetCorrectSum() {
        IntegerMath bitAddition = Mockito.mock(IntegerMath.class);
        int base = 10;
        String a = "99";
        String b = "102";
        String returnValue = "203";
        when(bitAddition.Add(a, b, base)).thenReturn(returnValue);
        assertEquals(bitAddition.Add(a, b, base), returnValue);
    }

    @Test
    public void shoudGetCorrectProduct() {
        IntegerMath multiply = Mockito.mock(IntegerMath.class);
        int base = 10;
        String a = "50";
        String b = "50";
        String returnValue = "2500";
        when(multiply.Multiply(a, b, base)).thenReturn(returnValue);
        assertEquals(multiply.Multiply(a, b, base), returnValue);

    }

}
